package assignment1;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Main {

    public static WebDriver driver;

    public static void main(String[] args) throws IOException, ParseException, FileNotFoundException {

        FileInputStream file = new FileInputStream("/Users/nikhilyadav/Documents/Coding/Java/Automation Practice/SeleniumAssignment/src/assignment1/configuration.properties");
        Properties properties = new Properties();
        properties.load(file);

        if (properties.getProperty("browser").equals("chrome")) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        } else if (properties.getProperty("browser").equals("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        }

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get(properties.getProperty("url"));

        JSONParser parser = new JSONParser();
        FileReader fileReader = new FileReader("/Users/nikhilyadav/Documents/Coding/Java/Automation Practice/SeleniumAssignment/src/assignment1/data.json");
        Object object = parser.parse(fileReader);
        JSONObject jsonObject = (JSONObject) object;

//        SignUp signUp = new SignUp(driver);
//        signUp.signUpSteps(jsonObject);

        SignIn signIn = new SignIn(driver);
        signIn.signInSteps(jsonObject);

        driver.quit();

        Screenshot screenshot = new Screenshot();
        screenshot.takeScreenshot();

    }

}
