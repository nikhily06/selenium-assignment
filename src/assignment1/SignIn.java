package assignment1;

import org.json.simple.JSONObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignIn extends PageInitialisation {

    public SignIn(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "a.login")
    WebElement signInButton;

    @FindBy(css = "input#email")
    WebElement emailInput;

    @FindBy(css = "input#passwd")
    WebElement passwordInput;

    public void pressSignInButton() {
        signInButton.click();
    }

    public void enterEmail(String email) {
        emailInput.sendKeys(email);
    }

    public void enterPassword(String password) {
        passwordInput.sendKeys(password + Keys.RETURN);
    }

    public void signInSteps(JSONObject jo) {
        pressSignInButton();
        enterEmail((String) jo.get("email"));
        enterPassword((String) jo.get("password"));
    }

}
