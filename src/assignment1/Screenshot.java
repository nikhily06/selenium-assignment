package assignment1;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class Screenshot {

    public void takeScreenshot() {

        EventFiringWebDriver driver;

        WebDriverManager.chromedriver().setup();
        driver = new EventFiringWebDriver(new ChromeDriver());

        Listener listener = new Listener();

        driver.register(listener);
        driver.get("http://automationpractice.com/index.php");
        driver.findElement(By.cssSelector("a.login")).click();

        driver.quit();

    }

}
