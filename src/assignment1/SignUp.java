package assignment1;

import org.json.simple.JSONObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class SignUp extends PageInitialisation {

    public SignUp(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "a.login")
    WebElement signInButton;

    @FindBy(css = "input#email_create")
    WebElement emailInput;

    @FindBy(css = "input#id_gender1")
    WebElement genderRadioButton;

    @FindBy(css = "input#customer_firstname")
    WebElement firstNameInput;

    @FindBy(css = "input#customer_lastname")
    WebElement lastNameInput;

    @FindBy(css = "input#passwd")
    WebElement passwordInput;

    @FindBy(css = "select#days")
    Select days;

    @FindBy(css = "select#months")
    Select months;

    @FindBy(css = "select#years")
    Select years;

    @FindBy(css = "input#firstname")
    WebElement firstName;

    @FindBy(css = "input#lastname")
    WebElement lastName;

    @FindBy(css = "input#address1")
    WebElement address;

    @FindBy(css = "input#city")
    WebElement city;

    @FindBy(css = "select#id_state")
    Select state;

    @FindBy(css = "input#postcode")
    WebElement postcode;

    @FindBy(css = "select#id_country")
    Select country;

    @FindBy(css = "input#phone_mobile")
    WebElement phone;

    @FindBy(css = "input#alias")
    WebElement addressAlias;

    @FindBy(css = "input#submitAccount")
    WebElement submit;

    public void pressSignInButton() {
        signInButton.click();
    }

    public void enterEmail(String email) {
        emailInput.sendKeys(email + Keys.RETURN);
    }

    public void selectGender() {
        genderRadioButton.click();
    }

    public void enterFirstname(String firstNameValue) {
        firstNameInput.sendKeys(firstNameValue);
    }

    public void enterLastname(String lastNameValue) {
        lastNameInput.sendKeys(lastNameValue);
    }

    public void enterPassword(String password) {
        passwordInput.sendKeys(password);
    }

    public void selectDay(String day) {
        days.selectByValue(day);
    }

    public void selectMonth(String month) {
        months.selectByValue(month);
    }

    public void selectYear(String year) {
        years.selectByValue(year);
    }

    public void enterFirstnameAgain(String firstNameValue) {
        firstName.sendKeys(firstNameValue);
    }

    public void enterLastnameAgain(String lastNameValue) {
        lastName.sendKeys(lastNameValue);
    }

    public void enterAddress(String addressInput) {
        address.sendKeys(addressInput);
    }

    public void enterCity(String cityValue) {
        city.sendKeys(cityValue);
    }

    public void selectState(String stateValue) {
        state.selectByValue(stateValue);
    }

    public void enterPostcode(String postcodeValue) {
        postcode.sendKeys(postcodeValue);
    }

    public void selectCountry(String countryValue) {
        country.selectByValue(countryValue);
    }

    public void enterPhone(String phoneValue) {
        phone.sendKeys(phoneValue);
    }

    public void enterAddressAlias(String alias) {
        addressAlias.sendKeys(alias);
    }

    public void pressSubmit() {
        submit.click();
    }

    public void signUpSteps(JSONObject jo) {
        pressSignInButton();
        enterEmail((String) jo.get("email"));
        selectGender();
        enterFirstname((String) jo.get("firstname"));
        enterLastname((String) jo.get("lastname"));
        enterPassword((String) jo.get("password"));
        selectDay((String) jo.get("day"));
        selectMonth((String) jo.get("month"));
        selectYear((String) jo.get("year"));
        enterFirstnameAgain((String) jo.get("firstname"));
        enterLastnameAgain((String) jo.get("lastname"));
        enterAddress((String) jo.get("address"));
        enterCity((String) jo.get("city"));
        selectState((String) jo.get("stateId"));
        enterPostcode((String) jo.get("postcode"));
        selectCountry((String) jo.get("countryId"));
        enterPhone((String) jo.get("phone"));
        enterAddressAlias((String) jo.get("alias"));
        pressSubmit();
    }

}
