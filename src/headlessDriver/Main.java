package headlessDriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Main {

    public static void main(String[] args) {

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");

        WebDriverManager.chromedriver().setup();

        WebDriver driver = new ChromeDriver(options);

        driver.get("https://google.com");

        System.out.println(driver.getTitle());

        if (driver.getTitle().equals("Google")) {
            System.out.println("Headless Browser test passed");
        }

    }

}
